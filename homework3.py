#Exercise 1
A = {1,2,3,4,5,7}
B = {2,4,5,9,12,24}
C = {2,4,8}
A.add(2)
A.add(4)
A.add(8)
print(A)

B.add(2)
B.add(4)
B.add(8)
print(B)

A.intersection(B)
A.union(B)
A.difference(B)

s1=str(A)
s2=str(B)
len(s1 + s2)
max(A.union(B))
min(A.union(B))

#Exercise 2
t=(1, 'python', [2,3], (4,5))
x,y=[2,3]
a,b=(4,5)
t=(1, 'python', x,y,a,b)
t[-1]
t1= t + ([2,3],)
print(t1)
l in t1
l1=list(t1)
print(l1)
l1.remove([2,3])
l1

#Exercise 3
#3.1
dic1={1:10,2:20}
dic2={3:30,4:40}
dic3={5:50,6:60}
d = dict(dic1)
d.update(dic2)
d.update(dic3)
print(d)
n=list(range(1,16))
m=[i**2 for i in n]
d1=dict(zip(n,m))
print(d1)

#3.2
d2={'a': 1, 'b': 4, 'c': 2}
d3=list(d2.items())
d3.sort(key=lambda x:x[1])
print(d3)

#3.3
s="Python is an easy language to learn"
dict((c,s.count(c)) for c in set(s))